package com.edsincloud.visita.controller;


import com.edsincloud.visita.model.Persona;
import com.edsincloud.visita.payload.PagedResponse;
import com.edsincloud.visita.service.PersonaService;
import com.edsincloud.visita.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/personas")
public class PersonaController {


    @Autowired
    private PersonaService personaService;

    @GetMapping
    public ResponseEntity<PagedResponse<Persona>> getAllTags(
            @RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {

        PagedResponse<Persona> response = personaService.getAllPersonas(page, size);
        return new ResponseEntity< >(response, HttpStatus.OK);
    }

}
