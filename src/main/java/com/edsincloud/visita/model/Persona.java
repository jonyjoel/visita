package com.edsincloud.visita.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "persona")
public class Persona {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "ap_paterno")
    private String ap_paterno;

    @Column(name = "ap_materno")
    private String ap_materno;

}
