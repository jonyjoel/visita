package com.edsincloud.visita.service.impl;


import com.edsincloud.visita.model.Persona;
import com.edsincloud.visita.payload.PagedResponse;
import com.edsincloud.visita.repository.PersonaRepository;
import com.edsincloud.visita.service.PersonaService;
import com.edsincloud.visita.utils.AppUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


@Service
public class PersonaServiceImpl implements PersonaService {


    @Autowired
    private PersonaRepository personaRepository;


    @Override
    public PagedResponse<Persona> getAllPersonas(int page, int size) {
        AppUtils.validatePageNumberAndSize(page, size);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");

        Page<Persona> personas = personaRepository.findAll(pageable);

        for (Persona per : personas){
            System.out.println("cantidad de registros:"+ per.getNombre());
        }


        List<Persona> content = personas.getNumberOfElements() == 0 ? Collections.emptyList() : personas.getContent();

        return new PagedResponse<>(content, personas.getNumber(), personas.getSize(), personas.getTotalElements(), personas.getTotalPages(), personas.isLast());
    }
}
