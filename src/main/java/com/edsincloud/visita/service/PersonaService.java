package com.edsincloud.visita.service;


import com.edsincloud.visita.model.Persona;
import com.edsincloud.visita.payload.PagedResponse;

public interface PersonaService<T> {

    PagedResponse<Persona> getAllPersonas(int page, int size);

}
