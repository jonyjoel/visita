package com.edsincloud.visita;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisitaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VisitaApplication.class, args);
	}

}
